package ua.training.model.service;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import ua.training.model.dao.VisitDao;
import ua.training.model.entity.User;
import ua.training.model.entity.Visit;
import ua.training.model.enumeration.Role;
import ua.training.model.enumeration.Speciality;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VisitServiceTest {
    @InjectMocks
    VisitService visitService = new VisitService();

    @Mock
    VisitDao visitDao;

    private static Visit visit;

    @BeforeClass
    public static void beforeClass(){
        User doctor = new User("Firstname", "Lastname", "username", "password", Role.ROLE_DOCTOR, Speciality.ALLERGIST);
        visit = new Visit(1, Date.valueOf("2020-12-12"),"09:00", doctor, 3, "conclusion");
    }

    @Test(expected = NullPointerException.class)
    public void create() {
        Mockito.doThrow(NullPointerException.class).doNothing().when(visitDao).create(null);
        visitService.create(null);
    }

    @Test(expected = NullPointerException.class)
    public void createMany() {
        Mockito.doThrow(NullPointerException.class).doNothing().when(visitDao).createMany(0,null,null);
        visitService.createMany(0,null,null);
    }

    @Test
    public void findAll() {
        when(visitDao.findAll()).thenReturn(Arrays.asList(visit));
        Assert.assertEquals(1, visitService.findAll().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void delete() {
        Mockito.doThrow(IllegalArgumentException.class).doNothing().when(visitDao).delete(0);
        visitService.delete(0);
    }

    @Test
    public void findFreeDatesToDoctor() {
        List<Date> dates = Arrays.asList(Date.valueOf("2020-12-12"), Date.valueOf("2020-12-13"));
        when(visitDao.findFreeDatesToDoctor(1)).thenReturn(dates);
        Assert.assertEquals(2, visitService.findFreeDatesToDoctor(1).size());
    }

    @Test
    public void findFreeHoursByDoctorIdAndDate() {
        List<String> hours = Arrays.asList("09:00", "10:00");
        when(visitDao.findFreeHoursByDoctorIdAndDate(1, Date.valueOf("2020-12-12"))).thenReturn(hours);
        Assert.assertEquals(2, visitService.findFreeHoursByDoctorIdAndDate(1, Date.valueOf("2020-12-12")).size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void recordPatientInVisit() {
        Mockito.doThrow(IllegalArgumentException.class).doNothing().when(visitDao).recordPatientInVisit(0,0,null,null);
        visitService.recordPatientInVisit(0,0,null,null);
    }

    @Test
    public void findByPatientId() {
        visit.setPatient_id(5);
        when(visitDao.findByPatientId(5)).thenReturn(Arrays.asList(visit));
        Assert.assertEquals(1, visitService.findByPatientId(5).size());
    }

    @Test
    public void findByDoctorId() {
        User doctor = visit.getDoctor();
        doctor.setId(10);
        visit.setDoctor(doctor);
        when(visitDao.findByDoctorId(10)).thenReturn(Arrays.asList(visit));
        Assert.assertEquals(1, visitService.findByDoctorId(10).size());
    }

    @Test
    public void findBookedByDoctorIdAndDate() {
        when(visitDao.findBookedByDoctorIdAndDate(10, Date.valueOf("2020-12-12"))).thenReturn(Arrays.asList(visit));
        Assert.assertEquals(1, visitService.findBookedByDoctorIdAndDate(10, Date.valueOf("2020-12-12")).size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void cancelVisit() {
        Mockito.doThrow(IllegalArgumentException.class).doNothing().when(visitDao).cancelVisit(0);
        visitService.cancelVisit(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void recordResult() {
        Mockito.doThrow(IllegalArgumentException.class).doNothing().when(visitDao).recordResult(0,null);
        visitService.recordResult(0, null);
    }

    @Test
    public void findById() {
        visit.setId(2);
        when(visitDao.findById(2)).thenReturn(visit);
        Assert.assertEquals(visit, visitService.findById(2));
    }
}