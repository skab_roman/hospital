package ua.training.model.service;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import ua.training.model.dao.UserDao;
import ua.training.model.entity.User;
import ua.training.model.enumeration.Role;
import ua.training.model.enumeration.Speciality;

import java.util.Arrays;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    UserService userService = new UserService();

    @Mock
    UserDao userDao;

    private static User user;

    @BeforeClass
    public static void beforeClass(){
        user = new User("Firstname", "Lastname", "username", "password", Role.ROLE_DOCTOR, Speciality.ALLERGIST);

    }

    @Test(expected = NullPointerException.class)
    public void create() {
        Mockito.doThrow(NullPointerException.class).doNothing().when(userDao).create(null);
        userService.create(null);
    }

    @Test
    public void findByUsernameAndPassword() {
        when(userDao.findByUsernameAndPassword("username", "password")).thenReturn(user);
        Assert.assertEquals(user, userService.findByUsernameAndPassword("username", "password"));
    }

    @Test(expected = NullPointerException.class)
    public void update() {
        Mockito.doThrow(NullPointerException.class).doNothing().when(userDao).update(null);
        userService.update(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void delete() {
        Mockito.doThrow(IllegalArgumentException.class).doNothing().when(userDao).delete(0);
        userService.delete(0);
    }

    @Test
    public void findByRole() {
        when(userDao.findByRole(Role.ROLE_DOCTOR)).thenReturn(Arrays.asList(user));
        Assert.assertEquals(Role.ROLE_DOCTOR, userService.findByRole(Role.ROLE_DOCTOR).get(0).getRole());
    }

    @Test
    public void findBySpeciality() {
        when(userDao.findBySpeciality(Speciality.ALLERGIST)).thenReturn(Arrays.asList(user));
        Assert.assertEquals(Speciality.ALLERGIST, userService.findBySpeciality(Speciality.ALLERGIST).get(0).getSpeciality());
    }

    @Test
    public void findById() {
        user.setId(1);
        when(userDao.findById(1)).thenReturn(user);
        Assert.assertEquals(user, userService.findById(1));
    }
}