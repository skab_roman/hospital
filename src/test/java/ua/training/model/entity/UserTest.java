package ua.training.model.entity;

import org.junit.BeforeClass;
import org.junit.Test;
import ua.training.model.enumeration.Gender;
import ua.training.model.enumeration.Role;
import ua.training.model.enumeration.Speciality;

import java.sql.Date;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class UserTest {
    private static User user;

    @BeforeClass
    public static void beforeClass(){
        user = new User();
    }

    @Test
    public void getId() {
        user.setId(1);
        assertEquals(1, user.getId());
    }

    @Test
    public void setId() {
        user.setId(2);
        assertEquals(2, user.getId());
    }

    @Test
    public void getFirstname() {
        user.setFirstname("Firstname");
        assertEquals("Firstname", user.getFirstname());
    }

    @Test
    public void setFirstname() {
        user.setFirstname("Firstname-2");
        assertEquals("Firstname-2", user.getFirstname());
    }

    @Test
    public void getLastname() {
        user.setLastname("Lastname");
        assertEquals("Lastname", user.getLastname());
    }

    @Test
    public void setLastname() {
        user.setLastname("Lastname-2");
        assertEquals("Lastname-2", user.getLastname());
    }

    @Test
    public void getGender() {
        user.setGender(Gender.FEMALE);
        assertEquals(Gender.FEMALE, user.getGender());
    }

    @Test
    public void setGender() {
        user.setGender(Gender.MALE);
        assertEquals(Gender.MALE, user.getGender());
    }

    @Test
    public void getBirthday() {
        user.setBirthday(Date.valueOf("2000-12-03"));
        assertEquals(Date.valueOf("2000-12-03"), user.getBirthday());
    }

    @Test(expected = IllegalArgumentException.class)
    public void setBirthday() {
        user.setBirthday(Date.valueOf("2000-21-03"));
    }

    @Test
    public void getLocation() {
        user.setLocation("Location");
        assertEquals("Location", user.getLocation());
    }

    @Test
    public void setLocation() {
        user.setLocation("Location-2");
        assertEquals("Location-2", user.getLocation());
    }

    @Test
    public void getUsername() {
        user.setUsername("Username");
        assertEquals("Username", user.getUsername());
    }

    @Test
    public void setUsername() {
        user.setUsername("Username-2");
        assertEquals("Username-2", user.getUsername());
    }

    @Test
    public void getPassword() {
        user.setPassword("password");
        assertEquals("password", user.getPassword());
    }

    @Test
    public void setPassword() {
        user.setPassword("password-2");
        assertEquals("password-2", user.getPassword());
    }

    @Test
    public void getRole() {
        user.setRole(Role.ROLE_PATIENT);
        assertEquals(Role.ROLE_PATIENT, user.getRole());
    }

    @Test
    public void setRole() {
        user.setRole(Role.ROLE_DOCTOR);
        assertEquals(Role.ROLE_DOCTOR, user.getRole());
    }

    @Test
    public void getSpeciality() {
        user.setSpeciality(Speciality.ALLERGIST);
        assertEquals(Speciality.ALLERGIST, user.getSpeciality());
    }

    @Test
    public void setSpeciality() {
        user.setSpeciality(Speciality.CARDIOLOGIST);
        assertNotEquals(Speciality.ALLERGIST, user.getSpeciality());
    }

    @Test
    public void getVisits() {
        user.setVisits(null);
        assertNull(user.getVisits());
    }

    @Test
    public void setVisits() {
        user.setVisits(new ArrayList<>());
        assertNotNull(user.getVisits());
    }
}