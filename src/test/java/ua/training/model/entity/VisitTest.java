package ua.training.model.entity;

import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Date;

import static org.junit.Assert.*;

public class VisitTest {
    private static Visit visit;

    @BeforeClass
    public static void beforeClass(){
        visit = new Visit();
    }

    @Test
    public void getId() {
        visit.setId(1);
        assertEquals(1, visit.getId());
    }

    @Test
    public void setId() {
        visit.setId(2);
        assertNotEquals(1, visit.getId());
    }

    @Test
    public void getDate() {
        visit.setDate(Date.valueOf("2020-01-01"));
        assertEquals(Date.valueOf("2020-01-01"), visit.getDate());
    }

    @Test(expected = IllegalArgumentException.class)
    public void setDate() {
        visit.setDate(Date.valueOf("2020-20-20"));
    }

    @Test
    public void getTime() {
        visit.setTime("09:00");
        assertEquals("09:00", visit.getTime());
    }

    @Test
    public void setTime() {
        visit.setTime("09:00");
        assertNotEquals("10:00", visit.getTime());
    }

    @Test
    public void getDoctor() {
        visit.setDoctor(null);
        assertNull(visit.getDoctor());
    }

    @Test
    public void setDoctor() {
        visit.setDoctor(new User());
        assertNotNull(visit.getDoctor());
    }

    @Test
    public void getPatient_id() {
        visit.setPatient_id(10);
        assertEquals(10, visit.getPatient_id());
    }

    @Test
    public void setPatient_id() {
        visit.setPatient_id(10);
        assertNotEquals(11, visit.getPatient_id());
    }

    @Test
    public void getConclusion() {
        visit.setConclusion("Conclusion");
        assertEquals("Conclusion", visit.getConclusion());
    }

    @Test
    public void setConclusion() {
        visit.setConclusion("Conclusion");
        assertNotEquals("Conclusion-2", visit.getConclusion());
    }
}