package ua.training.model.dao;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.training.model.dao.impl.UserDaoImpl;
import ua.training.model.entity.User;
import ua.training.model.enumeration.Role;
import ua.training.model.enumeration.Speciality;
import ua.training.util.JdbcUtil;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class UserDaoTest {
    private static UserDao userDao;

    private static DataSource dataSource;

    private static final String QUERY = "CREATE TABLE user(\n" +
            "            id INT PRIMARY KEY AUTO_INCREMENT,\n" +
            "            firstName VARCHAR(64) NOT NULL,\n" +
            "            lastName  VARCHAR(64) NOT NULL,\n" +
            "            gender VARCHAR(32),\n" +
            "            birthday DATE,\n" +
            "            location VARCHAR(64),\n" +
            "            username VARCHAR(64) NOT NULL UNIQUE,\n" +
            "            password VARCHAR(64) NOT NULL,\n" +
            "            role VARCHAR(32) NOT NULL,\n" +
            "            speciality VARCHAR(64)\n" +
            "            ); " +
            "INSERT INTO user (firstName, lastName, username, password, role)\n" +
            "VALUES ('Adminko', 'Anton', 'admin', '341a62a8fd29dff8fc5eaf45f624fc71', 'ROLE_ADMIN');\n" +
            "INSERT INTO user (firstName, lastName, username, password, role, speciality)\n" +
            "VALUES ('Doctorenko', 'Dmytro', 'd1', '1ff1b92d5fa8efad81bc80e732c28c3b', 'ROLE_DOCTOR', 'ALLERGIST');\n" +
            "INSERT INTO user (firstName, lastName, username, password, role, birthday)\n" +
            "VALUES ('Patientko', 'Petro', 'p1', 'd7c65ec4a9c55e2f73d743ec7c2ba355', 'ROLE_PATIENT', '1990-05-03');";

    private static final String DROP_TABLES = "DROP TABLE user";

    @BeforeClass
    public static void beforeClass() throws SQLException {
        dataSource = JdbcUtil.createDefaultInMemoryH2DataSource();
        createAndFillDBForTests(dataSource);
        userDao = new UserDaoImpl(dataSource.getConnection());
    }

    private static void createAndFillDBForTests(DataSource dataSource) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            conn.createStatement().execute(QUERY);
        }
    }

    @Test(expected = NullPointerException.class)
    public void create() {
        User user = new User();
        userDao.create(user);
    }

    @Test(expected = Exception.class)
    public void findById() {
        userDao.findById(0);
    }

    @Test(expected = NullPointerException.class)
    public void update() {
        User user = new User();
        userDao.update(user);
    }

    @Test(timeout = 100)
    public void delete() {
        userDao.delete(1);
    }

    @Test
    public void findByUsernameAndPassword() {
        User user = userDao.findByUsernameAndPassword("p1", "d7c65ec4a9c55e2f73d743ec7c2ba355");
        Assert.assertEquals("p1", user.getUsername());
    }

    @Test
    public void findByRole() {
        List<User> patients = userDao.findByRole(Role.ROLE_PATIENT);
        Assert.assertEquals(1, patients.size());
    }

    @Test
    public void findBySpeciality() {
        List<User> doctors = userDao.findBySpeciality(Speciality.ALLERGIST);
        Assert.assertEquals(1, doctors.size());
    }

    @AfterClass
    public static void afterClass() throws SQLException {
        dataSource.getConnection().createStatement().execute(DROP_TABLES);
    }
}