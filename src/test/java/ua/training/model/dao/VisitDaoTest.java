package ua.training.model.dao;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ua.training.exception.DaoOperationException;
import ua.training.model.dao.impl.VisitDaoImpl;
import ua.training.model.entity.Visit;
import ua.training.util.JdbcUtil;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;

public class VisitDaoTest {
    private static VisitDao visitDao;

    private static DataSource dataSource;

    private static final String QUERY = "" +
            "CREATE TABLE user(\n" +
            "            id INT PRIMARY KEY AUTO_INCREMENT,\n" +
            "            firstName VARCHAR(64) NOT NULL,\n" +
            "            lastName  VARCHAR(64) NOT NULL,\n" +
            "            gender VARCHAR(32),\n" +
            "            birthday DATE,\n" +
            "            location VARCHAR(64),\n" +
            "            username VARCHAR(64) NOT NULL UNIQUE,\n" +
            "            password VARCHAR(64) NOT NULL,\n" +
            "            role VARCHAR(32) NOT NULL,\n" +
            "            speciality VARCHAR(64)\n" +
            "            ); " +
            "INSERT INTO user (firstName, lastName, username, password, role)\n" +
            "VALUES ('Adminko', 'Anton', 'admin', '341a62a8fd29dff8fc5eaf45f624fc71', 'ROLE_ADMIN');\n" +
            "INSERT INTO user (firstName, lastName, username, password, role, speciality)\n" +
            "VALUES ('Doctorenko', 'Dmytro', 'd1', '1ff1b92d5fa8efad81bc80e732c28c3b', 'ROLE_DOCTOR', 'ALLERGIST');\n" +
            "INSERT INTO user (firstName, lastName, username, password, role, birthday)\n" +
            "VALUES ('Patientko', 'Petro', 'p1', 'd7c65ec4a9c55e2f73d743ec7c2ba355', 'ROLE_PATIENT', '1990-05-03'); " +
            "" +
            "CREATE TABLE visit(\n" +
            "            id INT PRIMARY KEY AUTO_INCREMENT,\n" +
            "            date DATE NOT NULL,\n" +
            "            time VARCHAR(32) NOT NULL,\n" +
            "            doctor_id INT,\n" +
            "            patient_id INT,\n" +
            "            conclusion VARCHAR(256),\n" +
            "            FOREIGN KEY (doctor_id) REFERENCES user (id) ON DELETE RESTRICT,\n" +
            "            FOREIGN KEY (patient_id) REFERENCES user (id) ON DELETE CASCADE\n" +
            "            ); " +
            "INSERT INTO visit(date, time, doctor_id, patient_id, conclusion) " +
            "VALUES ('2020-12-12', '09:00', 2, 3, 'conclusion is good')";

    private static final String DROP_TABLES = "DROP TABLE user, visit";

    @BeforeClass
    public static void beforeClass() throws SQLException {
        dataSource = JdbcUtil.createDefaultInMemoryH2DataSource();
        createAndFillDBForTests(dataSource);
        visitDao = new VisitDaoImpl(dataSource.getConnection());
    }

    private static void createAndFillDBForTests(DataSource dataSource) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            conn.createStatement().execute(QUERY);
        }
    }

    @Test(expected = NullPointerException.class)
    public void create() {
        Visit visit = new Visit();
        visitDao.create(visit);
    }

    @Test(expected = NullPointerException.class)
    public void createMany() {
        visitDao.createMany(0,null,null);
    }

    @Test
    public void findAll() {
        Assert.assertEquals(1, visitDao.findAll().size());
    }

    @Test
    public void findById() {
        Assert.assertEquals(1, visitDao.findById(1).getId());
    }

    @Test
    public void findByDoctorId() {
        Assert.assertEquals(1, visitDao.findByDoctorId(2).size());
    }

    @Test
    public void findBookedByDoctorIdAndDate() {
        Assert.assertEquals(1, visitDao.findBookedByDoctorIdAndDate(2, Date.valueOf("2020-12-12")).size());
    }

    @Test
    public void findByPatientId() {
        Assert.assertEquals(1, visitDao.findByPatientId(3).size());
    }

    @Test
    public void findFreeDatesToDoctor() {
        Assert.assertEquals(0, visitDao.findFreeDatesToDoctor(2).size());
    }

    @Test
    public void findFreeHoursByDoctorIdAndDate() {
        Assert.assertEquals(0, visitDao.findFreeHoursByDoctorIdAndDate(2, Date.valueOf("2020-12-12")).size());
    }

    @Test(expected = DaoOperationException.class)
    public void recordPatientInVisit() {
        visitDao.recordPatientInVisit(1,1,Date.valueOf("2020-12-12"),"10:00");
    }

    @Test(expected = DaoOperationException.class)
    public void cancelVisit() {
        visitDao.cancelVisit(0);
    }

    @Test(expected = DaoOperationException.class)
    public void recordResult() {
        visitDao.recordResult(2, "result");
    }

    @Test(expected = DaoOperationException.class)
    public void delete() {
        visitDao.delete(5);
    }

    @AfterClass
    public static void afterClass() throws SQLException {
        dataSource.getConnection().createStatement().execute(DROP_TABLES);
    }
}