package ua.training.model.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.exception.DaoOperationException;
import ua.training.model.dao.UserDao;
import ua.training.model.entity.User;
import ua.training.model.enumeration.Gender;
import ua.training.model.enumeration.Role;
import ua.training.model.enumeration.Speciality;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {
    private static final Logger logger = LogManager.getLogger(UserDaoImpl.class);

    private static final String INSERT_USER_SQL = "INSERT INTO " +
            "user(firstname, lastname, gender, birthday, location, username, password, role, speciality) VALUES(?,?,?,?,?,?,?,?,?);";
    private static final String SELECT_USER_BY_ID_SQL = "SELECT * FROM user WHERE user.id = ?;";
    private static final String SELECT_USER_BY_USERNAME_AND_PASSWORD = "SELECT * FROM user " +
            "WHERE username = ? AND password = ?;";
    private static final String SELECT_ALL_USERS_SQL = "SELECT * FROM user;";
    private static final String UPDATE_USER_SQL = "UPDATE user SET " +
            "firstname =?, lastname = ?, gender = ?, birthday = ?, location = ?, username = ?, password = ?" +
            "WHERE id = ?;";
    private static final String DELETE_USER_SQL = "DELETE FROM user WHERE id = ?";
    private static final String SELECT_USERS_BY_ROLE = "SELECT * FROM user WHERE user.role = ?";
    private static final String SELECT_USERS_BY_SPECIALITY = "SELECT * FROM user WHERE user.speciality = ?";

    private Connection connection;

    public UserDaoImpl(Connection connection) {
        this.connection = connection;
    }

    //    create user
    @Override
    public void create(User user) {
        try {
            createUser(user);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Can not create user!", e);
            logger.error("create() failed", exception);
            throw exception;
        }
    }

    private void createUser(User user) throws SQLException {
        PreparedStatement insertStatement = prepareInsertStatement(connection, user);
        executeUpdate(insertStatement, "User was not created!");
        int id = fetchGeneratedId(insertStatement);
        user.setId(id);
        logger.info("Created - " + user);
    }

    private PreparedStatement prepareInsertStatement(Connection connection, User user) {
        try {
            PreparedStatement insertStatement = connection.prepareStatement(INSERT_USER_SQL, PreparedStatement.RETURN_GENERATED_KEYS);
            return fillStatementWithUserData(insertStatement, user);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Can not prepare statement to insert user!", e);
            logger.error("prepareInsertStatement() failed", exception);
            throw exception;
        }
    }

    private PreparedStatement fillStatementWithUserData(PreparedStatement insertStatement, User user)
            throws SQLException {
        insertStatement.setString(1, user.getFirstname());
        insertStatement.setString(2, user.getLastname());
        try {
            insertStatement.setString(3, user.getGender().name());
        } catch (Exception e) {
            logger.info("Field 'gender' is empty!");
            insertStatement.setString(3, null);
        }
        insertStatement.setDate(4, user.getBirthday());
        insertStatement.setString(5, user.getLocation());
        insertStatement.setString(6, user.getUsername());
        insertStatement.setString(7, user.getPassword());
        insertStatement.setString(8, user.getRole().name());
        try {
            insertStatement.setString(9, user.getSpeciality().name());
        } catch (Exception e) {
            logger.info("Field 'speciality' is empty!");
            insertStatement.setString(9, null);
        }
        return insertStatement;
    }

    private void executeUpdate(PreparedStatement insertStatement, String errorMessage) throws SQLException {
        int rowsAffected = insertStatement.executeUpdate();
        if (rowsAffected == 0) {
            throw new DaoOperationException(errorMessage);
        }
    }

    private int fetchGeneratedId(PreparedStatement insertStatement) throws SQLException {
        ResultSet generatedKeys = insertStatement.getGeneratedKeys();
        if (generatedKeys.next()) {
            return generatedKeys.getInt(1);
        } else {
            throw new DaoOperationException("Cannot obtain an user ID");
        }
    }

    @Override
    public User findById(int id) {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_USER_BY_ID_SQL)) {
            ps.setInt(1, id);
            return executeFindBy(ps).get(0);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot find users by id", e);
            logger.error("findById() failed", exception);
            throw exception;
        }
    }

    //    update user
    @Override
    public void update(User user) {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_USER_SQL)) {
            ps.setString(1, user.getFirstname());
            ps.setString(2, user.getLastname());
            ps.setString(3, user.getGender().name());
            ps.setDate(4, user.getBirthday());
            ps.setString(5, user.getLocation());
            ps.setString(6, user.getUsername());
            ps.setString(7, user.getPassword());
            ps.setInt(8, user.getId());
            executeUpdate(ps, "Cannot update user with id = " + user.getId());
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot update user", e);
            logger.error("update() failed", exception);
            throw exception;
        }
    }

    //    delete user
    @Override
    public void delete(int id) {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_USER_SQL)) {
            ps.setInt(1, id);
            executeUpdate(ps, "Cannot delete user with id = " + id);
            logger.info("deleted user with id = " + id);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot delete user", e);
            logger.error("delete() failed", exception);
            throw exception;
        }
    }

    //    find user by username and password
    @Override
    public User findByUsernameAndPassword(String username, String password) {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_USER_BY_USERNAME_AND_PASSWORD)) {
            logger.info("UserDaoImpl: username - " + username + ", password - " + password);
            ps.setString(1, username);
            ps.setString(2, password);
            return executeFindByUsernameAndPassword(ps);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot find user by username and password", e);
            logger.error("findByUsernameAndPassword() failed", exception);
            throw exception;
        }
    }

    private User executeFindByUsernameAndPassword(PreparedStatement ps) {
        try (ResultSet rs = ps.executeQuery()) {
            rs.next();
            return parseRow(rs);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot executeFindByUsernameAndPassword", e);
            logger.error("executeFindByUsernameAndPassword() failed", exception);
            throw exception;
        }
    }

    private User parseRow(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setFirstname(rs.getString("firstname"));
        user.setLastname(rs.getString("lastname"));
        try {
            user.setGender(Gender.valueOf(rs.getString("gender")));
        } catch (Exception e) {
            logger.info("Cannot parse gender!");
        }
        try {
            user.setBirthday(rs.getDate("birthday"));
        } catch (Exception e) {
            logger.info("Cannot parse birthday!");
        }
        try {
            user.setLocation(rs.getString("location"));
        } catch (Exception e) {
            logger.info("Cannot parse location!");
        }
        user.setUsername(rs.getString("username"));
        user.setRole(Role.valueOf(rs.getString("role")));
        try {
            user.setSpeciality(Speciality.valueOf(rs.getString("speciality")));
        } catch (Exception e) {
            logger.info("Cannot parse speciality!");
        }
        return user;
    }

    //    find users by role
    @Override
    public List<User> findByRole(Role role) {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_USERS_BY_ROLE)) {
            ps.setString(1, role.name());
            return executeFindBy(ps);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot find users by role", e);
            logger.error("findByRole() failed", exception);
            throw exception;
        }
    }

    private List<User> executeFindBy(PreparedStatement ps) {
        try (ResultSet rs = ps.executeQuery()) {
            return collectToList(rs);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot executeFindBy", e);
            logger.error("executeFindBy() failed", exception);
            throw exception;
        }
    }

    private List<User> collectToList(ResultSet rs) throws SQLException {
        List<User> users = new ArrayList<>();
        while (rs.next()) {
            User account = parseRow(rs);
            users.add(account);
        }
        return users;
    }

    @Override
    public List<User> findBySpeciality(Speciality speciality) {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_USERS_BY_SPECIALITY)) {
            ps.setString(1, speciality.name());
            return executeFindBy(ps);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot find users by speciality", e);
            logger.error("findBySpeciality() failed", exception);
            throw exception;
        }
    }

}
