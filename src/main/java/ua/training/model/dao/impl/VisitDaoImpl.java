package ua.training.model.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.exception.DaoOperationException;
import ua.training.model.dao.VisitDao;
import ua.training.model.entity.User;
import ua.training.model.entity.Visit;
import ua.training.model.enumeration.Speciality;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class VisitDaoImpl implements VisitDao {
    private static final Logger logger = LogManager.getLogger(VisitDaoImpl.class);

    private static final String INSERT_VISIT_SQL = "INSERT INTO visit(date, time, doctor_id) VALUES(?,?,?);";
    private static final String SELECT_ALL_VISITS_SQL =
            "SELECT visit.*, user.speciality, user.firstname AS firstname, user.lastname AS lastname " +
                    "FROM visit " +
                    "JOIN user ON visit.doctor_id = user.id";
    private static final String DELETE_VISIT_SQL = "DELETE FROM visit WHERE id = ?";
    private static final String SELECT_FREE_DATES_TO_DOCTOR_SQL =
            "SELECT DISTINCT date FROM visit WHERE doctor_id = ? AND patient_id IS NULL";
    private static final String SELECT_FREE_HOURS_BY_DOCTOR_ID_AND_DATE_SQL =
            "SELECT time FROM visit WHERE doctor_id = ? AND date = ? AND patient_id IS NULL";
    private static final String UPDATE_VISIT_INSERT_PATIENT_ID_SQL = "UPDATE visit SET patient_id = ? " +
            "WHERE doctor_id = ? AND date = ? AND time = ?";
    private static final String SELECT_VISITS_BY_PATIENT_ID_SQL =
            "SELECT visit.*, user.speciality, user.firstname AS firstname, user.lastname AS lastname " +
                    "FROM visit " +
                    "JOIN user ON visit.doctor_id = user.id " +
                    "WHERE patient_id = ?";
    private static final String UPDATE_DELETE_PATIENT_FROM_VISIT = "UPDATE visit SET patient_id = NULL " +
            "WHERE id = ?";
    private static final String SELECT_VISITS_BY_DOCTOR_ID_SQL =
            "SELECT * FROM visit WHERE doctor_id = ?";
    private static final String SELECT_BOOKED_VISITS_BY_DOCTOR_ID_AND_DATE_SQL =
            "SELECT * FROM visit WHERE doctor_id = ? AND date = ? AND patient_id IS NOT NULL";
    private static final String UPDATE_RECORD_CONCLUSION = "UPDATE visit SET conclusion = ? WHERE id = ?";
    private static final String SELECT_VISIT_BY_ID_SQL = "SELECT * FROM visit WHERE id = ?";

    private Connection connection;

    public VisitDaoImpl(Connection connection) {
        this.connection = connection;
    }

    //    create one visit
    @Override
    public void create(Visit visit) {
        try (PreparedStatement ps = connection.prepareStatement(INSERT_VISIT_SQL, PreparedStatement.RETURN_GENERATED_KEYS)) {
            ps.setDate(1, visit.getDate());
            ps.setString(2, visit.getTime());
            ps.setInt(3, visit.getDoctor().getId());
            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 0) {
                throw new DaoOperationException("Visit was not created!");
            }
            int generatedId = fetchGeneratedId(ps);
            logger.info("rowsAffected - " + rowsAffected + ", generatedId - " + generatedId);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot create visit", e);
            logger.error("create() failed", exception);
            throw exception;
        }
    }

    @Override
    public void createMany(int doctorId, List<Date> dates, List<String> hours) {
        logger.info("createMany() started");
        try (PreparedStatement ps = connection.prepareStatement(INSERT_VISIT_SQL, PreparedStatement.RETURN_GENERATED_KEYS)) {
            for (Date date :
                    dates) {
                for (String hour : hours) {
                    ps.setDate(1, date);
                    ps.setString(2, hour);
                    ps.setInt(3, doctorId);
                    ps.addBatch();
                }
            }
            ps.executeBatch();
            ResultSet gk = ps.getGeneratedKeys();
            ArrayList<Integer> addedIdList = new ArrayList<>();
            while (gk.next()) {
                addedIdList.add(gk.getInt(1));
            }
            logger.info("Created schedule with id of visits: " + addedIdList);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot create many visits", e);
            logger.error("createMany() failed", exception);
            throw exception;
        }
    }

    private int fetchGeneratedId(PreparedStatement insertStatement) throws SQLException {
        ResultSet generatedKeys = insertStatement.getGeneratedKeys();
        if (generatedKeys.next()) {
            return generatedKeys.getInt(1);
        } else {
            throw new DaoOperationException("Cannot obtain an visit ID");
        }
    }

    @Override
    public void delete(int id) {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_VISIT_SQL)) {
            ps.setInt(1, id);
            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 0) {
                throw new DaoOperationException("Visit was not deleted!");
            }
            logger.info("deleted visit with id = " + id);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot delete user", e);
            logger.error("delete() failed", exception);
            throw exception;
        }
    }

    @Override
    public List<Visit> findAll() {
        try (Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_ALL_VISITS_SQL);
            ArrayList<Visit> visits = new ArrayList<>();
            while (rs.next()) {
                Visit visit = new Visit();
                visit.setId(rs.getInt("id"));
                visit.setDate(rs.getDate("date"));
                visit.setTime(rs.getString("time"));
                visit.setDoctor(new User(
                        rs.getString("firstname"),
                        rs.getString("lastname"),
                        Speciality.valueOf(rs.getString("speciality"))));
                visit.setPatient_id(rs.getInt("patient_id"));
                visit.setConclusion(rs.getString("conclusion"));
                visits.add(visit);
            }
            return visits;
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot execute findAll", e);
            logger.error("findAll() failed", exception);
            throw exception;
        }
    }

    @Override
    public Visit findById(int id) {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_VISIT_BY_ID_SQL)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            Visit visit = new Visit();
            if (rs.next()) {
                visit.setId(rs.getInt("id"));
                visit.setDate(rs.getDate("date"));
                visit.setTime(rs.getString("time"));
                visit.setPatient_id(rs.getInt("patient_id"));
                visit.setConclusion(rs.getString("conclusion"));
            }
            logger.info("found visit: ");
            return visit;
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot execute findById", e);
            logger.error("findById() failed", exception);
            throw exception;
        }
    }

    @Override
    public List<Visit> findByDoctorId(int id) {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_VISITS_BY_DOCTOR_ID_SQL)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            ArrayList<Visit> visits = new ArrayList<>();
            while (rs.next()) {
                Visit visit = new Visit();
                visit.setId(rs.getInt("id"));
                visit.setDate(rs.getDate("date"));
                visit.setTime(rs.getString("time"));
                visit.setPatient_id(rs.getInt("patient_id"));
                visit.setConclusion(rs.getString("conclusion"));
                visits.add(visit);
            }
            logger.info("found visits: ");
            return visits;
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot execute findByDoctorId", e);
            logger.error("findByDoctorId() failed", exception);
            throw exception;
        }
    }

    @Override
    public List<Visit> findBookedByDoctorIdAndDate(int doctorId, Date date) {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BOOKED_VISITS_BY_DOCTOR_ID_AND_DATE_SQL)) {
            ps.setInt(1, doctorId);
            ps.setDate(2, date);
            ResultSet rs = ps.executeQuery();
            ArrayList<Visit> visits = new ArrayList<>();
            while (rs.next()) {
                Visit visit = new Visit();
                visit.setId(rs.getInt("id"));
                visit.setDate(rs.getDate("date"));
                visit.setTime(rs.getString("time"));
                visit.setPatient_id(rs.getInt("patient_id"));
                visit.setConclusion(rs.getString("conclusion"));
                visits.add(visit);
            }
            logger.info("found visits: ");
            return visits;
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot execute findByDoctorIdAndDate", e);
            logger.error("findByDoctorIdAndDate() failed", exception);
            throw exception;
        }
    }

    @Override
    public List<Visit> findByPatientId(int id) {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_VISITS_BY_PATIENT_ID_SQL)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            ArrayList<Visit> visits = new ArrayList<>();
            while (rs.next()) {
                Visit visit = new Visit();
                visit.setId(rs.getInt("id"));
                visit.setDate(rs.getDate("date"));
                visit.setTime(rs.getString("time"));
                visit.setDoctor(new User(
                        rs.getString("firstname"),
                        rs.getString("lastname"),
                        Speciality.valueOf(rs.getString("speciality"))));
                visit.setPatient_id(rs.getInt("patient_id"));
                visit.setConclusion(rs.getString("conclusion"));
                visits.add(visit);
            }
            logger.info("found visits: ");
            return visits;
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot execute findByPatientId", e);
            logger.error("findByPatientId() failed", exception);
            throw exception;
        }
    }

    @Override
    public List<Date> findFreeDatesToDoctor(int doctorId) {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_FREE_DATES_TO_DOCTOR_SQL)) {
            ps.setInt(1, doctorId);
            ResultSet rs = ps.executeQuery();
            ArrayList<Date> dates = new ArrayList<>();
            while (rs.next()) {
                dates.add(rs.getDate("date"));
            }
            logger.info("free dates: " + dates);
            return dates;
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot execute findFreeDatesToDoctor", e);
            logger.error("findFreeDatesToDoctor() failed", exception);
            throw exception;
        }
    }

    @Override
    public List<String> findFreeHoursByDoctorIdAndDate(int doctorId, Date date) {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_FREE_HOURS_BY_DOCTOR_ID_AND_DATE_SQL)) {
            ps.setInt(1, doctorId);
            ps.setDate(2, date);
            ResultSet rs = ps.executeQuery();
            ArrayList<String> times = new ArrayList<>();
            while (rs.next()) {
                times.add(rs.getString("time"));
            }
            logger.info("free hours: " + times);
            return times;
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot execute findFreeHoursByDoctorIdAndDate", e);
            logger.error("findFreeHoursByDoctorIdAndDate() failed", exception);
            throw exception;
        }
    }

    @Override
    public void recordPatientInVisit(int patientId, int doctorId, Date date, String time) {
        logger.info("recordPatientInVisit() started");
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_VISIT_INSERT_PATIENT_ID_SQL)) {
            ps.setInt(1, patientId);
            ps.setInt(2, doctorId);
            ps.setDate(3, date);
            ps.setString(4, time);
            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 0) {
                throw new DaoOperationException("Visit was not updated!");
            }
            logger.info("rowsAffected - " + rowsAffected);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot update visit", e);
            logger.error("recordPatientInVisit() failed", exception);
            throw exception;
        }
    }

    @Override
    public void cancelVisit(int visitId) {
        logger.info("cancelVisit() started");
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_DELETE_PATIENT_FROM_VISIT)) {
            ps.setInt(1, visitId);
            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 0) {
                throw new DaoOperationException("Visit was not updated!");
            }
            logger.info("rowsAffected - " + rowsAffected);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot update visit", e);
            logger.error("cancelVisit() failed", exception);
            throw exception;
        }
    }

    @Override
    public void recordResult(int visitId, String conclusion) {
        logger.info("recordResult() started");
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_RECORD_CONCLUSION)) {
            ps.setString(1, conclusion);
            ps.setInt(2, visitId);
            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 0) {
                throw new DaoOperationException("Visit was not updated!");
            }
            logger.info("rowsAffected - " + rowsAffected);
        } catch (SQLException e) {
            DaoOperationException exception = new DaoOperationException("Cannot record result", e);
            logger.error("recordResult() failed", exception);
            throw exception;
        }
    }
}
