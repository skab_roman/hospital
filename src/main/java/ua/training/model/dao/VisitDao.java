package ua.training.model.dao;

import ua.training.model.entity.Visit;

import java.sql.Date;
import java.util.List;

public interface VisitDao {
    void create(Visit visit);

    void createMany(int doctorId, List<Date> dates, List<String> hours);

    void delete(int id);

    List<Visit> findAll();

    Visit findById(int id);

    List<Visit> findByDoctorId(int id);

    List<Visit> findBookedByDoctorIdAndDate(int doctorId, Date date);

    List<Visit> findByPatientId(int id);

    List<Date> findFreeDatesToDoctor(int doctorId);

    List<String> findFreeHoursByDoctorIdAndDate(int doctorId, Date date);

    void recordPatientInVisit(int patientId, int doctorId, Date date, String  time);

    void cancelVisit(int visitId);

    void recordResult(int visitId, String conclusion);

}
