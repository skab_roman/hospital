package ua.training.model.dao;

import ua.training.model.entity.User;
import ua.training.model.enumeration.Role;
import ua.training.model.enumeration.Speciality;

import java.util.List;

public interface UserDao{
    void create(User user);

    User findById(int id);

    void update(User user);

    void delete(int id);

    User findByUsernameAndPassword(String username, String password);

    List<User> findByRole(Role role);

    List<User> findBySpeciality(Speciality speciality);

}
