package ua.training.model.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.model.dao.VisitDao;
import ua.training.model.dao.impl.VisitDaoImpl;
import ua.training.model.entity.Visit;
import ua.training.util.JdbcUtil;

import java.sql.Date;
import java.util.List;

public class VisitService {
    private static final Logger logger = LogManager.getLogger(VisitService.class);

    private VisitDao visitDao = new VisitDaoImpl(JdbcUtil.getConnectionToH2DB());

    public void create(Visit visit){
        visitDao.create(visit);
    }

    public void createMany(int doctorId, List<Date> dates, List<String> hours){
        visitDao.createMany(doctorId, dates, hours);
    }

    public List<Visit> findAll(){
        return visitDao.findAll();
    }

    public void delete(int id){
        visitDao.delete(id);
    }

    public List<Date> findFreeDatesToDoctor(int doctorId){
        return visitDao.findFreeDatesToDoctor(doctorId);
    }

    public List<String> findFreeHoursByDoctorIdAndDate(int doctorId, Date date){
        return visitDao.findFreeHoursByDoctorIdAndDate(doctorId, date);
    }

    public void recordPatientInVisit(int patientId, int doctorId, Date date, String  time){
        visitDao.recordPatientInVisit(patientId, doctorId, date, time);
    }

    public List<Visit> findByPatientId(int id){
        return visitDao.findByPatientId(id);
    }

    public List<Visit> findByDoctorId(int id){
        return visitDao.findByDoctorId(id);
    }

    public List<Visit> findBookedByDoctorIdAndDate(int doctorId, Date date){
        return visitDao.findBookedByDoctorIdAndDate(doctorId, date);
    }

    public void cancelVisit(int visitId){
        visitDao.cancelVisit(visitId);
    }

    public void recordResult(int visitId, String conclusion){
        visitDao.recordResult(visitId, conclusion);
    }

    public Visit findById(int id){
        return visitDao.findById(id);
    }
}
