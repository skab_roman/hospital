package ua.training.model.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.model.dao.UserDao;
import ua.training.model.dao.impl.UserDaoImpl;
import ua.training.model.entity.User;
import ua.training.model.enumeration.Role;
import ua.training.model.enumeration.Speciality;
import ua.training.util.JdbcUtil;

import java.util.List;

public class UserService {
    private static final Logger logger = LogManager.getLogger(UserService.class);

    private UserDao userDao = new UserDaoImpl(JdbcUtil.getConnectionToH2DB());

    public void create(User user){
        userDao.create(user);
    }

    public User findByUsernameAndPassword(String username, String password){
        return userDao.findByUsernameAndPassword(username, password);
    }

    public void update(User user){
        userDao.update(user);
    }

    public void delete(int id){
        userDao.delete(id);
    }

    public List<User> findByRole(Role role){
        return userDao.findByRole(role);
    }

    public List<User> findBySpeciality(Speciality speciality){
        return userDao.findBySpeciality(speciality);
    }

    public User findById(int id){
        return userDao.findById(id);
    }
}
