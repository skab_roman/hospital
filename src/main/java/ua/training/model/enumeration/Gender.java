package ua.training.model.enumeration;

public enum Gender {
    MALE,
    FEMALE
}
