package ua.training.model.entity;

import java.sql.Date;
import java.util.Objects;

public class Visit {
    private int id;
    private Date date;
    private String time;
    private User doctor;
    private int patient_id;
    private String conclusion;

    public Visit() {
    }

    public Visit(int id, Date date, String time, User doctor, int patient_id, String conclusion) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.doctor = doctor;
        this.patient_id = patient_id;
        this.conclusion = conclusion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public User getDoctor() {
        return doctor;
    }

    public void setDoctor(User doctor) {
        this.doctor = doctor;
    }

    public int getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(int patient_id) {
        this.patient_id = patient_id;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Visit visit = (Visit) o;
        return id == visit.id &&
                patient_id == visit.patient_id &&
                date.equals(visit.date) &&
                time.equals(visit.time) &&
                doctor.equals(visit.doctor) &&
                conclusion.equals(visit.conclusion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, time, doctor, patient_id, conclusion);
    }

    @Override
    public String toString() {
        return "Visit{" +
                "id=" + id +
                ", date=" + date +
                ", time='" + time + '\'' +
                ", doctor=" + doctor +
                ", patient_id=" + patient_id +
                ", conclusion='" + conclusion + '\'' +
                '}';
    }
}
