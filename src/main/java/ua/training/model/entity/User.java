package ua.training.model.entity;

import ua.training.model.enumeration.Gender;
import ua.training.model.enumeration.Role;
import ua.training.model.enumeration.Speciality;

import java.sql.Date;
import java.util.List;
import java.util.Objects;

public class User {
    private int id;
    private String firstname;
    private String lastname;
    private Gender gender;
    private Date birthday;
    private String location;
    private String username;
    private String password;
    private Role role;
    private Speciality speciality;
    private List<Visit> visits;

    public User() {
    }

    public User(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public User(String firstname, String lastname, Speciality speciality) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.speciality = speciality;
    }

    public User(String firstname, String lastname, String username, String password) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
    }

    public User(String firstname, String lastname, String username, String password, Role role, Speciality speciality) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
        this.role = role;
        this.speciality = speciality;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public List<Visit> getVisits() {
        return visits;
    }

    public void setVisits(List<Visit> visits) {
        this.visits = visits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                firstname.equals(user.firstname) &&
                lastname.equals(user.lastname) &&
                gender == user.gender &&
                birthday.equals(user.birthday) &&
                location.equals(user.location) &&
                username.equals(user.username) &&
                password.equals(user.password) &&
                role == user.role &&
                speciality == user.speciality &&
                visits.equals(user.visits);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstname, lastname, gender, birthday, location, username, password, role, speciality, visits);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", gender=" + gender +
                ", birthday=" + birthday +
                ", location='" + location + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", speciality=" + speciality +
                ", visits=" + visits +
                '}';
    }
}
