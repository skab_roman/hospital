package ua.training.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.exception.UserAlreadyLoggedException;
import ua.training.exception.UserNotFoundException;
import ua.training.model.entity.User;
import ua.training.model.enumeration.Role;
import ua.training.model.service.UserService;
import ua.training.util.Encryptor;

import javax.servlet.http.HttpServletRequest;

public class Login implements Command {
    private static final Logger logger = LogManager.getLogger(Login.class);

    private UserService userService = new UserService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("Login -> execute()");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        logger.info("username: " + username + ", password: " + password);

        if (username == null || username.equals("") || password == null || password.equals("")) {
            return "/login.jsp";
        }

        if (CommandUtility.checkUserIsLogged(request, username)){
            UserAlreadyLoggedException exception = new UserAlreadyLoggedException("User already logged!");
            logger.error("This user already logged!", exception);
            throw exception;
        }

        User user;
        try {
            user = userService.findByUsernameAndPassword(username, Encryptor.encrypt(password));
        }catch (Exception e){
            UserNotFoundException exception = new UserNotFoundException("User not found!");
            logger.error("There is no user with such username and password!", exception);
            throw exception;
        }
        if (user.getRole().equals(Role.ROLE_PATIENT)) {
            CommandUtility.setUserInSession(request, user);
            CommandUtility.addUserToLoggedUsers(request, username);
            return "redirect:/patient";
        } else if (user.getRole().equals(Role.ROLE_DOCTOR)) {
            CommandUtility.setUserInSession(request, user);
            CommandUtility.addUserToLoggedUsers(request, username);
            return "redirect:/doctor";
        } else if (user.getRole().equals(Role.ROLE_ADMIN)) {
            CommandUtility.setUserInSession(request, user);
            CommandUtility.addUserToLoggedUsers(request, username);
            return "redirect:/admin";
        } else {
            return "/login.jsp";
        }


    }
}
