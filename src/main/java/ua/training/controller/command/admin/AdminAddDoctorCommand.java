package ua.training.controller.command.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.model.entity.User;
import ua.training.model.enumeration.Role;
import ua.training.model.enumeration.Speciality;
import ua.training.model.service.UserService;
import ua.training.util.Encryptor;

import javax.servlet.http.HttpServletRequest;

public class AdminAddDoctorCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminAddDoctorCommand.class);

    private UserService userService = new UserService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("AdminAddDoctorCommand -> execute()");

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String speciality = request.getParameter("speciality");
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (firstName == null || firstName.equals("")
                || lastName == null || lastName.equals("")
                || speciality == null || speciality.equals("")
                || username == null || username.equals("")
                || password == null || password.equals("")) {
            request.setAttribute("specialities", Speciality.values());
            return "/WEB-INF/admin/admin-addDoctor.jsp";
        } else {
            User user = new User();
            user.setFirstname(firstName);
            user.setLastname(lastName);
            user.setSpeciality(Speciality.valueOf(speciality));
            user.setUsername(username);
            user.setPassword(Encryptor.encrypt(password));
            user.setRole(Role.ROLE_DOCTOR);
            logger.info(user);

            userService.create(user);
            return "redirect:/admin/list-doctors";
        }
    }
}
