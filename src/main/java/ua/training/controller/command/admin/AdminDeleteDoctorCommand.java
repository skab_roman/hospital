package ua.training.controller.command.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.model.service.UserService;

import javax.servlet.http.HttpServletRequest;

public class AdminDeleteDoctorCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminDeleteDoctorCommand.class);

    private UserService userService = new UserService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("AdminDeleteDoctorCommand -> execute()");
        userService.delete(Integer.parseInt(request.getParameter("doctor-id")));
        return "redirect:/admin/list-doctors";
    }
}
