package ua.training.controller.command.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.model.entity.User;
import ua.training.model.enumeration.Role;
import ua.training.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AdminListDoctorsCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminListDoctorsCommand.class);

    private UserService userService = new UserService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("AdminListDoctorsCommand -> execute()");
        List<User> doctors = userService.findByRole(Role.ROLE_DOCTOR);
        request.setAttribute("doctors", doctors);

        return "/WEB-INF/admin/admin-listDoctors.jsp";
    }
}
