package ua.training.controller.command.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.model.service.VisitService;

import javax.servlet.http.HttpServletRequest;

public class AdminDeleteVisitCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminDeleteDoctorCommand.class);

    private VisitService visitService = new VisitService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("AdminDeleteVisitCommand -> execute()");
        visitService.delete(Integer.parseInt(request.getParameter("visit-id")));
        return "redirect:/admin/visits-schedule";
    }
}