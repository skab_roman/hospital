package ua.training.controller.command.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.exception.DataValidationException;
import ua.training.model.entity.User;
import ua.training.model.enumeration.Role;
import ua.training.model.service.UserService;
import ua.training.model.service.VisitService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AdminCreateScheduleCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminCreateScheduleCommand.class);

    private VisitService visitService = new VisitService();
    private UserService userService = new UserService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("AdminCreateScheduleCommand -> execute()");

        String doctorId = request.getParameter("doctorId");
        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");

        ArrayList<String> hours = new ArrayList<>();
        hours.add(request.getParameter("hour09"));
        hours.add(request.getParameter("hour10"));
        hours.add(request.getParameter("hour11"));
        hours.add(request.getParameter("hour12"));
        hours.add(request.getParameter("hour14"));
        hours.add(request.getParameter("hour15"));
        hours.add(request.getParameter("hour16"));
        hours.add(request.getParameter("hour17"));
        hours.removeIf(Objects::isNull);

        if (fromDate == null || fromDate.equals("")
                || toDate == null || toDate.equals("")
                || doctorId == null || doctorId.equals("")
                || hours.size() < 1
        ) {
            List<User> doctors = userService.findByRole(Role.ROLE_DOCTOR);
            request.setAttribute("doctors", doctors);
            return "/WEB-INF/admin/admin-createSchedule.jsp";
        } else {
            Date fromDateDate = Date.valueOf(fromDate);
            Date toDateDate = Date.valueOf(toDate);
            Date today = Date.valueOf(LocalDate.now());
            ArrayList<Date> dates = new ArrayList<>();
            if (fromDateDate.after(toDateDate)
            || fromDateDate.before(today)){
                throw new DataValidationException("Date is not valid!");
            }else {
                Date dateForList = fromDateDate;
                dates.add(dateForList);
                while (dateForList.before(toDateDate)){
                    LocalDate localDate = dateForList.toLocalDate().plusDays(1);
                    dateForList = Date.valueOf(localDate);
                    dates.add(dateForList);
                }
            }

            visitService.createMany(Integer.parseInt(doctorId), dates, hours);

            return "redirect:/admin/visits-schedule";
        }

    }
}
