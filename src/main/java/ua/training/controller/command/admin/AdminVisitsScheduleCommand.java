package ua.training.controller.command.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.model.entity.Visit;
import ua.training.model.service.VisitService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AdminVisitsScheduleCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminVisitsScheduleCommand.class);

    private VisitService visitService = new VisitService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("AdminVisitsScheduleCommand -> execute()");
        List<Visit> visits = visitService.findAll();
        request.setAttribute("visits", visits);

        return "/WEB-INF/admin/admin-visitsSchedule.jsp";
    }
}
