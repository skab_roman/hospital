package ua.training.controller.command.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;

import javax.servlet.http.HttpServletRequest;

public class AdminCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("AdminCommand -> execute()");

        return "WEB-INF/admin/admin-basis.jsp";
    }
}
