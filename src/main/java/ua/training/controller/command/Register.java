package ua.training.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.model.entity.User;
import ua.training.model.enumeration.Gender;
import ua.training.model.enumeration.Role;
import ua.training.model.service.UserService;
import ua.training.util.Encryptor;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

public class Register implements Command {
    private static final Logger logger = LogManager.getLogger(Register.class);

    private UserService userService = new UserService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("Register -> execute()");

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String gender = request.getParameter("gender");
        String birthday = request.getParameter("birthday");
        String location = request.getParameter("location");
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (firstName == null || firstName.equals("")
                || lastName == null || lastName.equals("")
                || gender == null || gender.equals("")
                || birthday == null || birthday.equals("")
                || location == null || location.equals("")
                || username == null || username.equals("")
                || password == null || password.equals("")) {
            return "/registration.jsp";
        } else {
            User user = new User();
            user.setFirstname(firstName);
            user.setLastname(lastName);
            user.setGender(Gender.valueOf(gender));
            user.setBirthday(Date.valueOf(birthday));
            user.setLocation(location);
            user.setUsername(username);
            user.setPassword(Encryptor.encrypt(password));
            user.setRole(Role.ROLE_PATIENT);
            logger.info(user);

            userService.create(user);

            return "redirect:/login.jsp";
        }

    }

}
