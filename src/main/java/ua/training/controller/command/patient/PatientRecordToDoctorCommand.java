package ua.training.controller.command.patient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.model.entity.User;
import ua.training.model.enumeration.Speciality;
import ua.training.model.service.UserService;
import ua.training.model.service.VisitService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.util.List;

public class PatientRecordToDoctorCommand implements Command {
    private static final Logger logger = LogManager.getLogger(PatientRecordToDoctorCommand.class);

    private UserService userService = new UserService();
    private VisitService visitService = new VisitService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("execute() started");

        String speciality = request.getParameter("speciality");
        String doctor = request.getParameter("doctor");
        String doctorId = request.getParameter("doctorId");
        String date = request.getParameter("date");
        String time = request.getParameter("time");

        if (speciality == null || speciality.equals("")) {
            request.setAttribute("specialities", Speciality.values());
        } else {
            List<User> doctors = userService.findBySpeciality(Speciality.valueOf(speciality));
            request.setAttribute("speciality", speciality);
            request.setAttribute("doctors", doctors);
        }

        if (doctor != null) {
            System.out.println("doctor - " + doctor);
            User doctor1 = userService.findById(Integer.parseInt(doctorId));
            List<Date> freeDates = visitService.findFreeDatesToDoctor(Integer.parseInt(doctorId));
            request.setAttribute("doctor", doctor1);
            request.setAttribute("doctorId", doctorId);
            request.setAttribute("dates", freeDates);
        }

        if (date != null && !date.equals("")) {
            User doctor1 = userService.findById(Integer.parseInt(doctorId));
            request.setAttribute("doctor", doctor1);
            request.setAttribute("doctorId", doctorId);
            request.setAttribute("date", date);

            List<String> hours = visitService.findFreeHoursByDoctorIdAndDate(Integer.parseInt(doctorId), Date.valueOf(date));
            request.setAttribute("hours", hours);
        }

        if (time != null && !time.equals("")){
            User patient = (User) request.getSession().getAttribute("user");
            visitService.recordPatientInVisit(patient.getId(), Integer.parseInt(doctorId), Date.valueOf(date), time);
            return "redirect:/patient/list-visits";
        }


        return "/WEB-INF/patient/patient-recordToDoctor.jsp";
    }
}
