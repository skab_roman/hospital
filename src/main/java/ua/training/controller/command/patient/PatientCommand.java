package ua.training.controller.command.patient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;

import javax.servlet.http.HttpServletRequest;

public class PatientCommand implements Command {
    private static final Logger logger = LogManager.getLogger(PatientCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("PatientCommand -> execute()");

        return "/WEB-INF/patient/patient-basis.jsp";
    }
}
