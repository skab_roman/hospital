package ua.training.controller.command.patient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.model.entity.User;
import ua.training.model.entity.Visit;
import ua.training.model.service.VisitService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class PatientListVisitsCommand implements Command {
    private static final Logger logger = LogManager.getLogger(PatientListVisitsCommand.class);

    private VisitService visitService = new VisitService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("execute() started");
        User user = (User) request.getSession().getAttribute("user");
        List<Visit> visits = visitService.findByPatientId(user.getId());
        request.setAttribute("visits", visits);
        return "/WEB-INF/patient/patient-listVisits.jsp";
    }
}
