package ua.training.controller.command.patient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.model.service.VisitService;

import javax.servlet.http.HttpServletRequest;

public class PatientCancelVisitCommand implements Command {
    private static final Logger logger = LogManager.getLogger(PatientCancelVisitCommand.class);

    private VisitService visitService = new VisitService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("execute() started");
        String visitId = request.getParameter("visit-id");
        visitService.cancelVisit(Integer.parseInt(visitId));
        return "redirect:/patient/list-visits";
    }
}
