package ua.training.controller.command.patient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.controller.command.CommandUtility;
import ua.training.model.entity.User;
import ua.training.model.service.UserService;

import javax.servlet.http.HttpServletRequest;

public class PatientDeleteCommand implements Command {
    private static final Logger logger = LogManager.getLogger(PatientDeleteCommand.class);

    private UserService userService = new UserService();

    @Override
    public String execute(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");
        userService.delete(user.getId());
        CommandUtility.removeUserFromSessionAndContext(request);
        return "redirect:/index.jsp";
    }
}
