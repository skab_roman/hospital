package ua.training.controller.command.doctor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.model.entity.User;
import ua.training.model.entity.Visit;
import ua.training.model.service.UserService;
import ua.training.model.service.VisitService;

import javax.servlet.http.HttpServletRequest;

public class DoctorReceptionCommand implements Command {
    private static final Logger logger = LogManager.getLogger(DoctorReceptionCommand.class);

    private UserService userService = new UserService();
    private VisitService visitService = new VisitService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("execute() started");

        String visitId = request.getParameter("visit-id");
        String conclusion = request.getParameter("conclusion");

        if (conclusion == null || conclusion.equals("")) {
            Visit visit = visitService.findById(Integer.parseInt(visitId));
            request.setAttribute("visit", visit);
            User patient = userService.findById(visit.getPatient_id());
            request.setAttribute("patient", patient);
            return "/WEB-INF/doctor/doctor-reception.jsp";
        }
        if (conclusion != null && !conclusion.equals("")) {
            visitService.recordResult(Integer.parseInt(visitId), conclusion);
            return "redirect:/doctor/today-visits";
        }
        return "/WEB-INF/doctor/doctor-reception.jsp";

    }
}
