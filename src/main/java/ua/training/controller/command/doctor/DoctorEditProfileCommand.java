package ua.training.controller.command.doctor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.model.entity.User;
import ua.training.model.enumeration.Gender;
import ua.training.model.enumeration.Speciality;
import ua.training.model.service.UserService;
import ua.training.util.Encryptor;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

public class DoctorEditProfileCommand implements Command {
    private static final Logger logger = LogManager.getLogger(DoctorEditProfileCommand.class);

    private UserService userService = new UserService();

    @Override
    public String execute(HttpServletRequest request) {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String gender = request.getParameter("gender");
        String birthday = request.getParameter("birthday");
        String location = request.getParameter("location");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String speciality = request.getParameter("speciality");

        if (firstName == null || firstName.equals("")
                || lastName == null || lastName.equals("")
                || username == null || username.equals("")
                || password == null || password.equals("")
                || speciality == null || speciality.equals("")
        ) {
            request.setAttribute("specialities", Speciality.values());
            return "/WEB-INF/doctor/doctor-editProfile.jsp";
        } else {
            User user = (User) request.getSession().getAttribute("user");
            user.setFirstname(firstName);
            user.setLastname(lastName);
            try {
                user.setGender(Gender.valueOf(gender));
            } catch (IllegalArgumentException e) {
                user.setGender(null);
            }
            try {
                user.setBirthday(Date.valueOf(birthday));
            } catch (Exception e) {
                user.setBirthday(null);
            }
            user.setLocation(location);
            user.setUsername(username);
            user.setPassword(Encryptor.encrypt(password));
            user.setSpeciality(Speciality.valueOf(speciality));
            logger.info("prepared user for update - " + user);

            userService.update(user);
            return "redirect:/doctor";
        }
    }
}
