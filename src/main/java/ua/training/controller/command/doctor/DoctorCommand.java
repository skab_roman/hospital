package ua.training.controller.command.doctor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;

import javax.servlet.http.HttpServletRequest;

public class DoctorCommand implements Command {
    private static final Logger logger = LogManager.getLogger(DoctorCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("DoctorCommand -> execute()");

        return "/WEB-INF/doctor/doctor-basis.jsp";
    }
}
