package ua.training.controller.command.doctor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.model.entity.User;
import ua.training.model.entity.Visit;
import ua.training.model.service.UserService;
import ua.training.model.service.VisitService;

import javax.servlet.http.HttpServletRequest;

public class DoctorViewVisitCommand implements Command {
    private static final Logger logger = LogManager.getLogger(DoctorViewVisitCommand.class);

    private VisitService visitService = new VisitService();
    private UserService userService = new UserService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("execute() started");
        String visitId = request.getParameter("visit-id");
        Visit visit = visitService.findById(Integer.parseInt(visitId));
        User patient = userService.findById(visit.getPatient_id());
        request.setAttribute("visit", visit);
        request.setAttribute("patient", patient);

        return "/WEB-INF/doctor/doctor-viewVisit.jsp";
    }
}
