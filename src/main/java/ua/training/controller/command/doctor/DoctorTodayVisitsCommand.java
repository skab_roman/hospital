package ua.training.controller.command.doctor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.model.entity.User;
import ua.training.model.entity.Visit;
import ua.training.model.service.VisitService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class DoctorTodayVisitsCommand implements Command {
    private static final Logger logger = LogManager.getLogger(DoctorTodayVisitsCommand.class);

    VisitService visitService = new VisitService();

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("execute() started");
        User doctor = (User) request.getSession().getAttribute("user");
        Date dateToday = Date.valueOf(LocalDate.now());
        List<Visit> visits = visitService.findBookedByDoctorIdAndDate(doctor.getId(), dateToday);
        request.setAttribute("visits", visits);
        return "/WEB-INF/doctor/doctor-todayVisits.jsp";
    }
}
