package ua.training.controller.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class Logout implements Command {
    private static final Logger logger = LogManager.getLogger(Logout.class);

    @Override
    public String execute(HttpServletRequest request) {
        logger.info("Logout -> execute() started");
        CommandUtility.removeUserFromSessionAndContext(request);
        return "/login.jsp";
    }
}
