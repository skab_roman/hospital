package ua.training.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.controller.command.Command;
import ua.training.controller.command.Login;
import ua.training.controller.command.Logout;
import ua.training.controller.command.Register;
import ua.training.controller.command.admin.*;
import ua.training.controller.command.doctor.*;
import ua.training.controller.command.patient.*;
import ua.training.util.InitialFillingDB;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/")
public class Servlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(Servlet.class);

    private Map<String, Command> commands = new HashMap<>();

    @Override
    public void init() {
        logger.info("Servlet -> init()");
        commands.put("register", new Register());
        commands.put("login", new Login());
        commands.put("logout", new Logout());
        commands.put("patient", new PatientCommand());
        commands.put("patient/edit-profile", new PatientEditProfileCommand());
        commands.put("patient/record-to-doctor", new PatientRecordToDoctorCommand());
        commands.put("patient/delete", new PatientDeleteCommand());
        commands.put("patient/list-visits", new PatientListVisitsCommand());
        commands.put("patient/cancel-visit", new PatientCancelVisitCommand());
        commands.put("doctor", new DoctorCommand());
        commands.put("doctor/edit-profile", new DoctorEditProfileCommand());
        commands.put("doctor/list-visits", new DoctorListVisitsCommand());
        commands.put("doctor/today-visits", new DoctorTodayVisitsCommand());
        commands.put("doctor/reception", new DoctorReceptionCommand());
        commands.put("doctor/view-visit", new DoctorViewVisitCommand());
        commands.put("admin", new AdminCommand());
        commands.put("admin/list-doctors", new AdminListDoctorsCommand());
        commands.put("admin/add-doctor", new AdminAddDoctorCommand());
        commands.put("admin/delete-doctor", new AdminDeleteDoctorCommand());
        commands.put("admin/list-patients", new AdminListPatientsCommand());
        commands.put("admin/visits-schedule", new AdminVisitsScheduleCommand());
        commands.put("admin/delete-visit", new AdminDeleteVisitCommand());
        commands.put("admin/create-schedule", new AdminCreateScheduleCommand());

        InitialFillingDB.fill();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {
        System.out.println("Servlet - method GET");
        processRequest(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        System.out.println("Servlet - method POST");
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = request.getRequestURI();
        logger.info("RequestURI - " + path);
        path = path.replaceAll(".*/hospital/", "");
        logger.info("path: " + path);
        Command command = commands.getOrDefault(path,
                (r) -> "/hospital/index.jsp");
        String page = command.execute(request);
        logger.info("page - " + page);
        if (page.contains("redirect:")) {
            response.sendRedirect(page.replace("redirect:", "/hospital"));
        } else {
            logger.info("forwarded to page - " + page);
            request.getRequestDispatcher(page).forward(request, response);
        }
    }

}
