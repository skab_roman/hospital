package ua.training.controller.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.training.exception.ForbiddenException;
import ua.training.model.entity.User;
import ua.training.model.enumeration.Role;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.stream.Stream;

@WebFilter("/*")
public class AccessFilter implements Filter {
    private static final Logger logger = LogManager.getLogger(AccessFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        logger.info("AccessFilter -> doFilter()");
        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession();

        String path = req.getRequestURI();
        logger.info("path - " + path);

        if (session.getAttribute("user") == null){
            Stream<String> urls = Stream.of(
                    "/hospital/",
                    "/hospital/register.jsp",
                    "/hospital/login.jsp",
                    "/hospital/register",
                    "/hospital/login");
            if(urls.noneMatch(path::equals)){
                HttpServletResponse resp = (HttpServletResponse) response;
                resp.sendRedirect("/hospital/");
                return;
            }

        }

        if (session.getAttribute("user") != null && !path.equals("/hospital/logout")) {
            User user = (User) session.getAttribute("user");
            logger.info("current user: " + user);

            Role role = user.getRole();
            String roleInStringPathFormat = role.name().toLowerCase().substring(5);
            logger.info("roleInStringPathFormat - " + roleInStringPathFormat);

            if (path.equals("/hospital/")) {
                HttpServletResponse resp = (HttpServletResponse) response;
                resp.sendRedirect("/hospital/" + roleInStringPathFormat);
                return;
            }

            if (!path.contains(roleInStringPathFormat)) {
                ForbiddenException exception = new ForbiddenException("User doesn't have access to the resource");
                logger.error("Access denied !!!", exception);
                throw exception;
            }
        }

        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
