package ua.training.exception;

public class DBConnectException extends RuntimeException{
    public DBConnectException(String message) {
        super(message);
    }

    public DBConnectException(String message, Throwable cause) {
        super(message, cause);
    }
}
