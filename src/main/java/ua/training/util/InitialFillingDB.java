package ua.training.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.stream.Collectors;

public class InitialFillingDB {
    private static final String CREATE_TABLE_SQL = "CREATE TABLE user(" +
            "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT," +
            "firstName VARCHAR(30) NOT NULL," +
            "lastName  VARCHAR(30) NOT NULL," +
            "gender VARCHAR(30)," +
            "birthday DATE," +
            "location VARCHAR(30)," +
            "username VARCHAR(30) NOT NULL," +
            "password VARCHAR(30) NOT NULL," +
            "role VARCHAR(30) NOT NULL," +
            ");";
    private static final String INSERT_SQL = "INSERT INTO message(body) VALUES (?)";
    private static final String SELECT_ALL_SQL = "SELECT * FROM message";

    public static void fill() {
        new InitialFillingDB().startFill();
    }

    private static void createUserTable() throws SQLException {
        try (Connection connection = JdbcUtil.getConnectionToH2DB()) {
            Statement statement = connection.createStatement();
            statement.execute(CREATE_TABLE_SQL);
        }

    }

    public void startFill() {
        try {
            createUserTable2();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createUserTable2() throws SQLException, IOException {
        try (Connection connection = JdbcUtil.getConnectionToH2DB();
             InputStream reader = (this.getClass().getResourceAsStream("/sql/initialFillingDB.sql"));
        ) {
            Statement statement = connection.createStatement();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(reader));
            String CREATE_TABLE = bufferedReader.lines().collect(Collectors.joining());

            statement.execute(CREATE_TABLE);

        }

    }

    private static void saveSomeMessagesIntoDB() throws SQLException {
        try (Connection connection = JdbcUtil.getConnectionToH2DB()) {
            PreparedStatement insertStatement = connection.prepareStatement(INSERT_SQL);
            insertSomeMessages(insertStatement);
        }
    }

    private static void insertSomeMessages(PreparedStatement insertStatement) throws SQLException {
        insertStatement.setString(1, "Hello!");
        insertStatement.executeUpdate();

        insertStatement.setString(1, "How are you?");
        insertStatement.executeUpdate();
    }

    private static void printMessagesFromDB() throws SQLException {
        //try-with-resource will automatically close Connection resource
        try (Connection connection = JdbcUtil.getConnectionToH2DB()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_ALL_SQL);
            printAllMessages(resultSet);
        }
    }

    private static void printAllMessages(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            String messageText = resultSet.getString(1);
            Timestamp timestamp = resultSet.getTimestamp(2);
            System.out.println(" - " + messageText + " [" + timestamp + "]");
        }
    }
}
