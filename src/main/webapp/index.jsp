<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hospital</title>
</head>
<body>
<h1>Virtual hospital</h1>
<br>
<h3>Welcome to our page!</h3>
<h4>Please, select the next step:</h4>
<form action="${pageContext.request.contextPath}/login.jsp">
    <button>Login</button>
</form>
<br>
<form action="${pageContext.request.contextPath}/register.jsp">
    <button>Register</button>
</form>
</body>
<style>
    form{
        margin-top: 20px;
    }
    body{
        margin: 0;
        background-color: lightgray;
        text-align: center;
    }
</style>
</html>
