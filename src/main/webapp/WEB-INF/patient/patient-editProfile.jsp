<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit profile</title>
</head>
<body>
<form method="post" action="${pageContext.request.contextPath}/patient/edit-profile">
    <b>Edit profile</b>
    <br><br>
    Name: <input name="firstName" value="${user.firstname}"/>
    <br><br>
    Surname: <input name="lastName" value="${user.lastname}"/>
    <br><br>
    Gender:
    <input type="radio" name="gender" value="MALE" checked/>MALE
    <input type="radio" name="gender" value="FEMALE"/>FEMALE
    <br><br>

    Birthday: <input type="date" name="birthday" value="${user.birthday}"/>
    <br><br>
    City: <input name="location" value="${user.location}"/>
    <br><br>

    Email: <input name="username" value="${user.username}"/>
    <br><br>
    Password: <input name="password" type="password" min=1/>
    <br><br>
    <input type="submit" value="Submit"/>
</form>

<br>
<form method="post" action="${pageContext.request.contextPath}/patient/delete">
    <button>Delete me</button>
</form>

<br><br><br>
<a href="${pageContext.request.contextPath}/patient">Home page</a>
</body>
<style>
    form{
        margin-top: 20px;
    }
    body{
        margin: 0;
        background-color: lightgray;
        text-align: center;
    }
</style>
</html>
