<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Record</title>
</head>
<body>
<b>Record to doctor:</b>
<br><br>
<form action="${pageContext.request.contextPath}/patient/record-to-doctor" method="post">
    Speciality:
    <c:if test="${speciality == null}">
        <select name="speciality">
            <c:forEach var="speciality" items="${specialities}">
                <option>${speciality}</option>
            </c:forEach>
        </select>
        <button>next</button>
    </c:if>
    ${speciality}
    <input type="hidden" name="speciality" value="${speciality}">

    <br><br>
    Doctor:
    <c:if test="${speciality != null && doctor == null}">
        <select name="doctorId">
            <c:forEach var="doctor" items="${doctors}">
                <option value="${doctor.id}">${doctor.firstname} ${doctor.lastname}</option>
                <input type="hidden" name="doctor" value="${doctor}">
            </c:forEach>
        </select>
        <button>next</button>
    </c:if>
    ${doctor.firstname} ${doctor.lastname}
    <input type="hidden" name="doctorId" value="${doctorId}">

    <br><br>
    Date:
    <c:if test="${doctor != null && date == null}">
        <select name="date">
            <c:forEach var="date" items="${dates}">
                <option>${date}</option>
            </c:forEach>
        </select>
        <button>next</button>
    </c:if>
    ${date}
    <input type="hidden" name="date" value="${date}">

    <br><br>
    Time:
    <c:if test="${date != null && time == null}">
        <select name="time">
            <c:forEach var="time" items="${hours}">
                <option>${time}</option>
            </c:forEach>
        </select>
        <button>record</button>
    </c:if>
</form>

<br><br><br>
<a href="${pageContext.request.contextPath}/patient">Home page</a>
</body>
</html>
