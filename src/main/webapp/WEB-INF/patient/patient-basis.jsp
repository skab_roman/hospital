<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Patient</title>
</head>
<body>
<h4>Patient home page</h4>
<h3>Name: ${user.firstname} ${user.lastname}
    <br><small>City: ${user.location} Email: ${user.username}</small>
</h3>



<a href="${pageContext.request.contextPath}/patient/list-visits">My visits</a>
<br><br>
<a href="${pageContext.request.contextPath}/patient/record-to-doctor">Record to doctor</a>
<br><br>
<a href="${pageContext.request.contextPath}/patient/edit-profile">Edit profile</a>
<br><br>
<a href="${pageContext.request.contextPath}/logout">Logout</a>

</body>
<style>
    body {
        margin: 0;
        background-color: lightgray;
        text-align: center;
    }
</style>
</html>
