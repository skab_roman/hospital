<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Visits</title>
</head>
<body>
List visits:
<table>
    <thead>
    <tr>
        <th>id</th>
        <th>speciality</th>
        <th>doctor</th>
        <th>date</th>
        <th>time</th>
        <th>conclusion</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="visit" items="${visits}">
        <form action="${pageContext.request.contextPath}/patient/cancel-visit" method="post">
            <tr>
                <td><c:out value="${visit.id}"/></td>
                <td><c:out value="${visit.doctor.speciality}"/></td>
                <td><c:out value="${visit.doctor.firstname}  ${visit.doctor.lastname}"/></td>
                <td><c:out value="${visit.date}"/></td>
                <td><c:out value="${visit.time}"/></td>
                <td><c:out value="${visit.conclusion}"/></td>
                <td><c:if test="${visit.conclusion == null}">
                    <input hidden type="text" name="visit-id" value="${visit.id}"><input type="submit" value="cancel">
                </c:if></td>
            </tr>
        </form>
    </c:forEach>
    </tbody>
</table>

<br><br><br>
<a href="${pageContext.request.contextPath}/patient">Home page</a>
</body>
</html>
