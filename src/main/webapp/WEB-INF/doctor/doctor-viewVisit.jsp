<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View visit</title>
</head>
<body>
Visit:
<br><br>
Id: ${visit.id}
<br>
Date: ${visit.date}
<br>
Time: ${visit.time}
<br>
Patient: ${patient.firstname} ${patient.lastname}
<br>
<c:if test="${visit.conclusion != null}">
    Conclusion: ${visit.conclusion}
</c:if>

<br><br><br>
<a href="${pageContext.request.contextPath}/doctor">Home page</a>
</body>
</html>
