<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Reception</title>
</head>
<body>
<b>Reception:</b>
<br><br>
Date: ${visit.date}
<br>
Time: ${visit.time}
<br>
Patient: ${patient.firstname} ${patient.lastname}
<br><br>
Conclusion:
<form action="${pageContext.request.contextPath}/doctor/reception" method="post">
    <input type="text" hidden name="visit-id" value="${visit.id}">
    <textarea name="conclusion" rows="3"></textarea>
    <br>
    <button>Save</button>
</form>
</body>
</html>
