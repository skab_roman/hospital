<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Visits</title>
</head>
<body>
List visits:
<table>
    <thead>
    <tr>
        <th>id</th>
        <th>date</th>
        <th>time</th>
        <th>patient_id</th>
        <th>conclusion</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="visit" items="${visits}">
        <form action="${pageContext.request.contextPath}/doctor/view-visit" method="post">
            <tr>
                <td><c:out value="${visit.id}"/></td>
                <td><c:out value="${visit.date}"/></td>
                <td><c:out value="${visit.time}"/></td>
                <td><c:if test="${visit.patient_id == 0}"><p> - </p></c:if>
                    <c:if test="${visit.patient_id != 0}"><p>${visit.patient_id}</p></c:if></td>
                <td><c:out value="${visit.conclusion}"/></td>
                <td><c:if test="${visit.patient_id != 0}">
                    <input hidden type="text" name="visit-id" value="${visit.id}"><input type="submit" value="view">
                </c:if></td>
            </tr>
        </form>
    </c:forEach>
    </tbody>
</table>

<br><br><br>
<a href="${pageContext.request.contextPath}/doctor">Home page</a>
</body>

<style>
    td {
        text-align: center;
    }
</style>
</html>
