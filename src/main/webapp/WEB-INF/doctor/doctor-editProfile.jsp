<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit profile</title>
</head>
<body>
<form method="post" action="${pageContext.request.contextPath}/doctor/edit-profile">
    <b>Edit profile</b>
    <br><br>
    Name: <input name="firstName" value="${user.firstname}"/>
    <br><br>
    Surname: <input name="lastName" value="${user.lastname}"/>
    <br><br>
    Speciality:
    <select name="speciality">
        <c:forEach var="speciality" items="${specialities}">
            <option>${speciality}</option>
        </c:forEach>
    </select>
    <br><br>
    Gender:
    <input type="radio" name="gender" value="MALE" checked/>MALE
    <input type="radio" name="gender" value="FEMALE"/>FEMALE
    <br><br>

    Birthday: <input type="date" name="birthday" value="${user.birthday}"/>
    <br><br>
    City: <input name="location" value="${user.location}"/>
    <br><br>

    Email: <input name="username" value="${user.username}"/>
    <br><br>
    Password: <input name="password" type="password" min=1/>
    <br><br>
    <input type="submit" value="Submit"/>
</form>

<br><br><br>
<a href="${pageContext.request.contextPath}/doctor">Home page</a>
</body>
</html>
