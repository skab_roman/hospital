<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Doctor</title>
</head>
<body>
<h4>Doctor home page</h4>
<h3>Name: ${user.firstname} ${user.lastname}
    <br>${user.speciality}</h3>

<br>
<a href="${pageContext.request.contextPath}/doctor/today-visits">Reception today</a>
<br><br>
<a href="${pageContext.request.contextPath}/doctor/list-visits">List visits</a>
<br><br>
<a href="${pageContext.request.contextPath}/doctor/edit-profile">Edit profile</a>
<br><br>
<a href="${pageContext.request.contextPath}/logout">Logout</a>
</body>
<style>
    body{
        margin: 0;
        background-color: lightgray;
        text-align: center;
    }
</style>
</html>
