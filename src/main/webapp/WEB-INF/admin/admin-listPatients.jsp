<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin/patients</title>
</head>
<body>
List patients:
<table>
    <thead>
    <tr>
        <th>id</th>
        <th>name</th>
        <th>surname</th>
        <th>birthday</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="patient" items="${patients}">
        <tr>
            <td><c:out value="${patient.id}"/></td>
            <td><c:out value="${patient.firstname}"/></td>
            <td><c:out value="${patient.lastname}"/></td>
            <td><c:out value="${patient.birthday}"/></td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<br><br><br>
<a href="${pageContext.request.contextPath}/admin">Home page</a>
</body>
</html>
