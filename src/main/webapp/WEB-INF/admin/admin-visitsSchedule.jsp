<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Schedule</title>
</head>
<body>
Schedule of visits
<table>
    <thead>
    <tr>
        <th>id</th>
        <th>speciality</th>
        <th>doctor</th>
        <th>date</th>
        <th>time</th>
        <th>patient_id</th>
        <th>conclusion</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="visit" items="${visits}">
        <form action="${pageContext.request.contextPath}/admin/delete-visit" method="post">
            <tr>
                <td><c:out value="${visit.id}"/></td>
                <td><c:out value="${visit.doctor.speciality}"/></td>
                <td><c:out value="${visit.doctor.firstname}  ${visit.doctor.lastname}"/></td>
                <td><c:out value="${visit.date}"/></td>
                <td><c:out value="${visit.time}"/></td>
                <td><c:if test="${visit.patient_id == 0}"><p> - </p></c:if>
                    <c:if test="${visit.patient_id != 0}"><p>${visit.patient_id}</p></c:if></td>
                <td><c:out value="${visit.conclusion}"/></td>
                <td><input hidden type="text" name="visit-id" value="${visit.id}"><input type="submit" value="delete"></td>
            </tr>
        </form>
    </c:forEach>
    </tbody>
</table>

<br><br>
<a href="${pageContext.request.contextPath}/admin/create-schedule">Create schedule</a>

<br><br><br>
<a href="${pageContext.request.contextPath}/admin">Home page</a>
</body>

<style>
    td {
        text-align: center;
    }
</style>
</html>
