<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin/addDoctor</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/admin/add-doctor" method="post">
    <b>Doctor registration</b>
    <br><br>
    Name: <input name="firstName"/>
    <br><br>
    Surname: <input name="lastName"/>
    <br><br>
    Speciality:
    <select name="speciality">
        <c:forEach var="speciality" items="${specialities}">
            <option>${speciality}</option>
        </c:forEach>
    </select>
    <br><br>

    Email/username: <input name="username"/>
    <br><br>
    Password: <input name="password" type="password" min=1/>
    <br><br>
    <input type="submit" value="Submit"/>
</form>

<br><br><br>
<a href="${pageContext.request.contextPath}/admin">Home page</a>
</body>
</html>
