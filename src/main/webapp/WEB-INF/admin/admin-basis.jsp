<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
</head>
<body>
<h4>Admin home page</h4>
<h3>Name: ${user.firstname} ${user.lastname}</h3>

<br>
<a href="${pageContext.request.contextPath}/admin/list-doctors">List doctors</a>

<br><br>
<a href="${pageContext.request.contextPath}/admin/list-patients">List patients</a>

<br><br>
<a href="${pageContext.request.contextPath}/admin/visits-schedule">Visits schedule</a>

<br><br><br>
<a href="${pageContext.request.contextPath}/logout">Logout</a>
</body>
<style>
    body{
        margin: 0;
        background-color: lightgray;
        text-align: center;
    }
</style>
</html>
