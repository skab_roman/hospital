<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Creating schedule</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/admin/create-schedule" method="post">
    <b>Creating schedule</b>
    <br><br>
    Doctor:
    <select name="doctorId">
        <c:forEach var="doctor" items="${doctors}">
            <option value="${doctor.id}">${doctor.speciality} ${doctor.firstname} ${doctor.lastname}</option>
        </c:forEach>
    </select>
    <br><br>
    Period:
    <br>
    from <input type="date" name="fromDate"><span>   </span> to <input type="date" name="toDate">
    <br><br>
    Hours:
    <br>
    <input type="checkbox" name="hour09" value="09:00">09:00
    <input type="checkbox" name="hour10" value="10:00">10:00
    <br>
    <input type="checkbox" name="hour11" value="11:00">11:00
    <input type="checkbox" name="hour12" value="12:00">12:00
    <br>
    <input type="checkbox" name="hour14" value="14:00">14:00
    <input type="checkbox" name="hour15" value="15:00">15:00
    <br>
    <input type="checkbox" name="hour16" value="16:00">16:00
    <input type="checkbox" name="hour17" value="17:00">17:00
    <br><br>
    <input type="submit" value="Submit"/>
</form>
</body>
</html>
