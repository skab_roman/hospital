<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin/doctors</title>
</head>
<body>
List doctors:

<table>
    <thead>
    <tr>
        <th>id</th>
        <th>speciality</th>
        <th>name</th>
        <th>surname</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="doctor" items="${doctors}">
        <form action="${pageContext.request.contextPath}/admin/delete-doctor" method="post">
            <tr>
                <td><c:out value="${doctor.id}"/><input name="doctor-id" type="hidden" value="${doctor.id}"></td>
                <td><c:out value="${doctor.speciality}"/></td>
                <td><c:out value="${doctor.firstname}"/></td>
                <td><c:out value="${doctor.lastname}"/></td>
                <td><input type="submit" value="delete"></td>
            </tr>
        </form>
    </c:forEach>
    </tbody>
</table>

<br><br>
<a href="${pageContext.request.contextPath}/admin/add-doctor">Add new doctor</a>

<br><br><br>
<a href="${pageContext.request.contextPath}/admin">Home page</a>
</body>
</html>
