<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/register" method="post">
    <b>Patient registration</b>
    <br><br>
    Name: <input name="firstName"/>
    <br><br>
    Surname: <input name="lastName"/>
    <br><br>
    Gender:
    <input type="radio" name="gender" value="MALE" checked/>MALE
    <input type="radio" name="gender" value="FEMALE"/>FEMALE
    <br><br>

    Birthday: <input type="date" name="birthday"/>
    <br><br>
    City: <input name="location"/>
    <br><br>

    Email: <input name="username"/>
    <br><br>
    Password: <input name="password" type="password" min=1/>
    <br><br>
    <input type="submit" value="Submit"/>
</form>

<br>
<a href="${pageContext.request.contextPath}/index.jsp">Start page</a>
</body>
<style>
    form{
        margin-top: 20px;
    }
    body{
        margin: 0;
        background-color: lightgray;
        text-align: center;
    }
</style>
</html>
