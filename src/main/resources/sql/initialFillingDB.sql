CREATE TABLE user(
            id INT PRIMARY KEY AUTO_INCREMENT,
            firstName VARCHAR(64) NOT NULL,
            lastName  VARCHAR(64) NOT NULL,
            gender VARCHAR(32),
            birthday DATE,
            location VARCHAR(64),
            username VARCHAR(64) NOT NULL UNIQUE,
            password VARCHAR(64) NOT NULL,
            role VARCHAR(32) NOT NULL,
            speciality VARCHAR(64)
            );

CREATE TABLE visit(
            id INT PRIMARY KEY AUTO_INCREMENT,
            date DATE NOT NULL,
            time VARCHAR(32) NOT NULL,
            doctor_id INT,
            patient_id INT,
            conclusion VARCHAR(256),
            FOREIGN KEY (doctor_id) REFERENCES user (id) ON DELETE RESTRICT,
            FOREIGN KEY (patient_id) REFERENCES user (id) ON DELETE CASCADE
            );

INSERT INTO user (firstName, lastName, username, password, role)
VALUES ('Adminko', 'Anton', 'admin', '341a62a8fd29dff8fc5eaf45f624fc71', 'ROLE_ADMIN');

INSERT INTO user (firstName, lastName, username, password, role, speciality)
VALUES ('Doctorenko', 'Dmytro', 'd1', '1ff1b92d5fa8efad81bc80e732c28c3b', 'ROLE_DOCTOR', 'ALLERGIST');

INSERT INTO user (firstName, lastName, username, password, role, birthday)
VALUES ('Patientko', 'Petro', 'p1', 'd7c65ec4a9c55e2f73d743ec7c2ba355', 'ROLE_PATIENT', '1990-05-03');